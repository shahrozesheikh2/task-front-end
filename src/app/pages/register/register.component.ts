import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { paths } from 'src/environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public roles = [];
  public registrationForm  = this.fb.group({
    firt_name : [null,Validators.required],
    last_name :  [null,Validators.required],
    email : [null,[Validators.required,Validators.email]],
    password : [null,Validators.required],
    roleId : [null,Validators.required]
  })
  constructor(
    private fb: FormBuilder,
    private httpClient : HttpClient,
    private router: Router) { }

  ngOnInit(): void {
    this.getRoles();
  }
  public getRoles(){
    this.httpClient.get(
      paths.roles
    )
    .subscribe(
      (response: any) => {
        this.roles = response.roles;
      }
    )
  }
  selectRole(event : any){
    this.registrationForm.controls['roleId'].setValue(event.target.value)
  }

  public submit(){

    this.registrationForm.markAllAsTouched()
    if(this.registrationForm.valid){
      var body ={
        "email":  this.registrationForm.controls['email'].value,
        "password":this.registrationForm.controls['password'].value,
        "firstName":this.registrationForm.controls['firt_name'].value,
        "lastLame":this.registrationForm.controls['last_name'].value,
        "roleId": this.registrationForm.controls['roleId'].value
      }
      console.log(JSON.stringify(body))
      this.httpClient.post(
        paths.signUp,
        body
      )
      .subscribe(
        (response : any) =>
        {
          if(response.message == "Sucessfully")
          {
            this.router.navigateByUrl("/login")
          }
        }
        ,error =>{
          alert("Email already exists!")
        }
      )
    }
  }
}
