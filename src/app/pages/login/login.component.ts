import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { paths } from 'src/environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public registrationForm  = this.fb.group({
    email : [null,[Validators.required,Validators.email]],
    password : [null,Validators.required]
  })
  constructor(
    private fb: FormBuilder,
    private httpClient : HttpClient,
    private router: Router) { }

  ngOnInit(): void {
  }
  public submit(){
    this.registrationForm.markAllAsTouched()
    if(this.registrationForm.valid){
      const body = {
        "email" : this.registrationForm.controls['email'].value ,
         "password":this.registrationForm.controls['password'].value
     }
     this.httpClient.post(
       paths.signIn,
       body
     )
     .subscribe(
       (response : any) =>
       {
         if(response.message === "Sucessfully"){
          
           alert("Logged in successfully!")
          localStorage.setItem("token",JSON.stringify(response.response.token))
           this.router.navigateByUrl("/home")
         }
         else{
          alert("Invalid credentials!")
         }
       }
     )
    }
  }
}
