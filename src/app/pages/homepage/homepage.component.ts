import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { paths } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  public numbers : any;
  public socketArry : any =[];
  timer : any;
  public generatedNumberForm  = this.fb.group({
    game_name : [null,Validators.required],
    starting_number : [null,Validators.required],
    ending_number : [null,Validators.required],
    remove_number : [null,Validators.required],
  })
  valuesArray: any;
 

  constructor(
    private fb: FormBuilder,
    private httpClient : HttpClient,
    private router: Router
  ) { }

  endTimer(){
    clearInterval(this.timer);
  }

  socketInterval(){
    this.timer = setInterval(
      () =>
      {
        if(this.numbers.length > 0){
          this.socketArry.push(this.numbers[0])
          this.numbers.splice(0,1)
        }
        else{
          this.endTimer();
        }
      }
    ,3000)
  }
  ngOnInit(): void {
  }

  public submit(){
    this.generatedNumberForm.markAllAsTouched()
    if(this.generatedNumberForm.valid){


    var valuesStr = this.generatedNumberForm.controls['remove_number'].value.split(" "); //split based on ' ' and store on a variable
    this.valuesArray = valuesStr.map((v: string) => parseInt(v)); //convert each item to int

      var body ={
        "generateNumber":  this.generatedNumberForm.controls['game_name'].value,
        "startingNumber":  this.generatedNumberForm.controls['starting_number'].value,
        "endingNumber":  this.generatedNumberForm.controls['ending_number'].value,
        "removeNumbers":this.valuesArray
      }
      
      let headers = new HttpHeaders();
      headers = headers.set("Authorization","Bearer "+JSON.parse(localStorage.getItem('token') || '{}'))
      console.log("headers"+headers)
      this.httpClient.post(
        paths.generatedNumber,
        body,
        {headers: headers}
      )
      .subscribe(
        (response : any) =>
        {
          if(response.message == "all")
          {
            alert("added successfully!")
            this.numbers = response.respone.numbers
            this.generatedNumberForm.reset()
            this.socketArry = []
            this.socketInterval();
            // this.router.navigateByUrl("/login")
          }
        }
        ,error =>{
          alert("not Added!")
        }
      )
    }
  }

}
