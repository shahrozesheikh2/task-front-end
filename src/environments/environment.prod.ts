export const environment = {
  production: true
};
export const paths ={
  roles : "https://gameinstance.herokuapp.com/users/roles",
  signUp : "https://gameinstance.herokuapp.com/users/signUp",
  signIn : "https://gameinstance.herokuapp.com/users/signIn",
}
